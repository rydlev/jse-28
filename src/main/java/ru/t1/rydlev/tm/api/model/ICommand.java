package ru.t1.rydlev.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.enumerated.Role;

public interface ICommand {

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @NotNull
    String getName();

    @Nullable
    Role[] getRoles();

    void execute();

}

package ru.t1.rydlev.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.command.data.AbstractDataCommand;
import ru.t1.rydlev.tm.command.data.DataBackupLoadCommand;
import ru.t1.rydlev.tm.command.data.DataBackupSaveCommand;
import ru.t1.rydlev.tm.component.Bootstrap;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class BackupExecutor {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public BackupExecutor(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        bootstrap.parseCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.parseCommand(DataBackupLoadCommand.NAME, false);
    }

}

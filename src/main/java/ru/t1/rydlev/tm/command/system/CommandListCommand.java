package ru.t1.rydlev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.api.model.ICommand;
import ru.t1.rydlev.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            final String name = command.getName();
            if (name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-c";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show developer commands.";
    }

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

}

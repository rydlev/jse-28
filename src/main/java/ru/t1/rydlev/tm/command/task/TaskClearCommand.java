package ru.t1.rydlev.tm.command.task;

import org.jetbrains.annotations.NotNull;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        getTaskService().clear();
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

}

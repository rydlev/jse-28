package ru.t1.rydlev.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + getPropertyService().getApplicationName());

        System.out.println("[DEVELOPER]");
        System.out.println("NAME: " + getPropertyService().getAuthorName());
        System.out.println("E-MAIL: " + getPropertyService().getAuthorEmail());

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + getPropertyService().getGitBranch());
        System.out.println("COMMIT ID: " + getPropertyService().getGitCommitId());
        System.out.println("COMMITER: " + getPropertyService().getGitCommitterName());
        System.out.println("E-MAIL: " + getPropertyService().getGitCommitterEmail());
        System.out.println("MESSAGE: " + getPropertyService().getGitCommitMessage());
        System.out.println("TIME: " + getPropertyService().getGitCommitTime());
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show developer info.";
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

}
